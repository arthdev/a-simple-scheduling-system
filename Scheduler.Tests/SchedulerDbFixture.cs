using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Scheduler.Infra;

namespace Scheduler.Tests
{
    // if it needs to be used by more than one TestClasses, 
    // it will need to declared as ICollectionFixture instead of as IClassFixture in TestClasses
    // see: 
    // - https://learn.microsoft.com/en-us/ef/core/testing/testing-with-the-database#tests-which-explicitly-manage-transactions
    // - https://xunit.net/docs/shared-context#collection-fixture
    public class SchedulerDbFixture : IDisposable
    {
        public SchedulerDbFixture()
        {
            Console.WriteLine("Creating Fixture " + this.GetType().FullName);
            this.Cleanup();
        }
        
        public AppDbContext CreateContext()
        {
            DbContextOptions<AppDbContext> options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(this.GetType().Name).Options;
            return new AppDbContext(options);
        }

        public void Cleanup()
        {
            Console.WriteLine("Executing Fixture Cleanup " + this.GetType().FullName);
            using (var context = this.CreateContext())
            {
                Console.WriteLine(context.Slots.Count() + " slots will be removed.");                
                context.Slots.RemoveRange(context.Slots);

                context.SaveChanges();
            }
        }

        public void Dispose()
        {
            Console.WriteLine("Disposing Fixture" + this.GetType().FullName);
            this.Cleanup();
        }
    }
}