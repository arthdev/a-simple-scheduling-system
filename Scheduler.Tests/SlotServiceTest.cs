using Xunit;
using Scheduler.Services;
using System.Collections.Generic;
using Scheduler.Events;
using System;
using Moq;
using Scheduler.Entities;

namespace Scheduler.Tests {
    public class SlotServiceTest
    {
        [Fact]
        public void ConfirmAppointment_NotEnoughSloghts_ThrowsException()
        {
            int appointmentDuration = 25;
            int requiredQuantityOfSlots = (appointmentDuration / Constants.SlotDuration);

            var dto = new AppointmentRequested();
            dto.AppointmentId = 1;
            dto.Duration = appointmentDuration;
            dto.Id = Guid.NewGuid();
            dto.ServiceId = 1;
            dto.StartDateTime = new DateTime(2023, 02, 12, 14, 15, 00);
            dto.Status = 1;

            var repo = GetRepositoryMock();
            var slotService = new Mock<SlotService>(repo.Object);
            repo.Setup(s => s.Get(It.IsAny<ICriteria<Slot>>())).Returns(
                Utils.CreateStraightSlots(dto.StartDateTime, (appointmentDuration - Constants.SlotDuration))
            );

            var exception = Assert.Throws<Exception>(() => slotService.Object.ConfirmAppointment(dto));
            Assert.Contains("There are not slots enough avaliable for this appointment!", exception.Message);
        }

        [Fact]
        public void ConfirmAppointment_EnoughSloghts_FillAppointmentId()
        {
            int appointmentDuration = 25;
            int requiredQuantityOfSlots = (appointmentDuration / Constants.SlotDuration);

            var dto = new AppointmentRequested();
            dto.AppointmentId = 1;
            dto.Duration = appointmentDuration;
            dto.Id = Guid.NewGuid();
            dto.ServiceId = 1;
            dto.StartDateTime = new DateTime(2023, 02, 12, 14, 15, 00);
            dto.Status = 1;
            
            var repo = GetRepositoryMock();
            var slotService = new SlotService(repo.Object);
            // You can use the property below if you want to 
            // call concrete class' "original" virtual methods if those methods are not mock explicitilly using .Setup().
            // mockedObjectWithSomeMethodsVirtual.CallBase = true;

            var slots = Utils.CreateStraightSlots(dto.StartDateTime, appointmentDuration);
            repo.Setup(s => s.Get(It.IsAny<ICriteria<Slot>>())).Returns(slots);

            slotService.ConfirmAppointment(dto);

            repo.Verify(c => c.Complete(), Times.Once());

            slots.ForEach(s => {
                Assert.NotNull(s.AppointmentId);
            });
        }

        private Mock<IRepository<Slot, int>> GetRepositoryMock() 
        {
            return new Mock<IRepository<Slot, int>>();
        }
    }
}