using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json.Nodes;
using Scheduler.Entities;
using Xunit;

namespace Scheduler.Tests;

public class Utils
{
    public static List<Slot> CreateStraightSlots(DateTime starting, int duration)
    {
        var slots = new List<Slot>();
        var quantity = (int) duration / Constants.SlotDuration;

        for (int i = 0; i < quantity; i++)
        {
            slots.Add(
                    new Slot() {
                    AppointmentId = null,
                    Duration = Constants.SlotDuration,
                    DateTime = starting.AddMinutes((i) * Constants.SlotDuration)
                }
            );
        }

        return slots;
    }   
}