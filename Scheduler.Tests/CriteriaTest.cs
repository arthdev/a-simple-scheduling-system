using Xunit;
using System;
using Scheduler.Infra;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Scheduler.Tests;
public class CriteriaTest : IClassFixture<SchedulerDbFixture>, IDisposable
{
    private readonly SchedulerDbFixture _fixture;

    public CriteriaTest(SchedulerDbFixture fixture)
    {
        Console.WriteLine("Creating Test Class " + this.GetType().FullName);

        _fixture = fixture;
    }

    [Fact]
    public void TestFilterByDay()
    {
        SlotCriteria criteria = new SlotCriteria();
        criteria.Day = new DateTime(2023, 02, 12, 00, 00, 00);

        var twoDaysSpan = new TimeSpan(2, 0, 0, 0).TotalMinutes;
        var slots = Utils.CreateStraightSlots(new DateTime(2023, 02, 12, 00, 00, 00), (int) twoDaysSpan);
        var repo = new SlotRepository(_fixture.CreateContext());

        foreach (var slot in slots)
        {
            repo.Add(slot);
        }

        repo.Complete();
        
        var filtered = repo.Get(criteria);

        foreach (var daySlot in filtered)
        {
            Assert.True(daySlot.DateTime.Day.Equals(criteria.Day.Value.Day));
            Assert.True(daySlot.DateTime.Month.Equals(criteria.Day.Value.Month));
            Assert.True(daySlot.DateTime.Year.Equals(criteria.Day.Value.Year));
        }
    }

    [Fact]
    public void TestFilterAvaliability()
    {
        var period = new TimeSpan(0, 10, 0).TotalMinutes;
        var busySlots = Utils.CreateStraightSlots(new DateTime(2023, 02, 12, 8, 00, 00), (int) period);
        var avaliableSlots = Utils.CreateStraightSlots(new DateTime(2023, 02, 12, 9, 00, 00), (int) period);

        var repo = new SlotRepository(_fixture.CreateContext());

        foreach (var slot in busySlots)
        {
            slot.AppointmentId = 1;
            repo.Add(slot);
        }

        foreach (var slot in avaliableSlots)
        {
            repo.Add(slot);
        }

        repo.Complete();

        SlotCriteria criteria = new SlotCriteria();
        criteria.IsAvaliable = true;
        var filtered = repo.Get(criteria);

        Assert.Equal(filtered.Count(), avaliableSlots.Count());

        foreach (var slot in avaliableSlots)
        {
            Assert.Contains(filtered, (fs) => slot.Id.Equals(fs.Id));
        }

        criteria = new SlotCriteria();
        criteria.IsAvaliable = false;        
        filtered = repo.Get(criteria);

        Assert.Equal(filtered.Count(), busySlots.Count());

        foreach (var slot in busySlots)
        {
            Assert.Contains(filtered, (fs) => slot.Id.Equals(fs.Id));
        }
    }

    [Fact]
    public void TestFilterByMinimumDateTime()
    {
        SlotCriteria criteria = new SlotCriteria();
        criteria.MinimumDateTime = new DateTime(2023, 2, 13, 14, 00, 00);

        var period = new TimeSpan(0, 10, 0).TotalMinutes;
        var matchingslots = Utils.CreateStraightSlots(criteria.MinimumDateTime.Value, (int) period);
        var nonMatchingslots = Utils.CreateStraightSlots(criteria.MinimumDateTime.Value.AddMinutes(period * -1), (int) period);

        var repo = new SlotRepository(_fixture.CreateContext());

        foreach (var item in matchingslots.Concat(nonMatchingslots))
        {

            repo.Add(item);
        }
        repo.Complete();

        var filtered = repo.Get(criteria);

        Assert.Equal(filtered.Count(), matchingslots.Count());

        foreach (var slot in matchingslots)
        {
            Assert.Contains(filtered, (fs) => slot.Id.Equals(fs.Id));
        }
    }

    [Fact]
    public void TestFilterByMaximumDateTime()
    {
        SlotCriteria criteria = new SlotCriteria();
        criteria.MaximumDateTime = new DateTime(2023, 2, 12, 14, 25, 00);        

        var period = new TimeSpan(0, 10, 0).TotalMinutes;
        var matchingslots = Utils.CreateStraightSlots(criteria.MaximumDateTime.Value.AddMinutes(period * -1 + Constants.SlotDuration), 
            (int) period);
        var nonMatchingslots = Utils.CreateStraightSlots(criteria.MaximumDateTime.Value.AddMinutes(Constants.SlotDuration),
            (int) period);

        var repo = new SlotRepository(_fixture.CreateContext());

        foreach (var item in matchingslots.Concat(nonMatchingslots))
        {
            repo.Add(item);
        }
        repo.Complete();

        var filtered = repo.Get(criteria);

        Assert.Equal(filtered.Count(), matchingslots.Count());

        foreach (var slot in matchingslots)
        {
            Assert.Contains(filtered, (fs) => slot.Id.Equals(fs.Id));
        }
    }

    public void Dispose()
    {        
        Console.WriteLine("Disposing Test Class " + this.GetType().FullName);
        _fixture.Cleanup();
    }   
}