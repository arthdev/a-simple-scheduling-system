using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MessageBus;

namespace Scheduling.Events
{
    public class AppointmentRequested
    {
        public Guid Id { get; set; }
        public int AppointmentId { get; set; }
        public DateTime StartDateTime { get; set; }
        public int? Duration { get; set; }
        public int? ServiceId { get; set; }
    }
}