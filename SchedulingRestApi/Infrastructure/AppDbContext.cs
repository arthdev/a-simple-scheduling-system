using Microsoft.EntityFrameworkCore;
using Scheduling.Entities;

namespace Scheduling.Infra
{
    

public class AppDbContext : DbContext
{
    public DbSet<Appointment> Appoitments { get; set; }
    public DbSet<Service> Services { get; set; }
    public DbSet<Slot> Slots { get; set; }

    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {

    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {     
        // service

        modelBuilder.Entity<Service>()
            .Property(s => s.Duration)
            .IsRequired();

        modelBuilder.Entity<Service>()
            .HasKey(s => s.Id);

        // appointment            

        modelBuilder.Entity<Appointment>()
            .HasKey(a => a.Id);
        
        modelBuilder.Entity<Appointment>()
            .Property(a => a.StartDateTime)
            .IsRequired();

        modelBuilder.Entity<Appointment>()
            .Property(a => a.Duration);

        modelBuilder.Entity<Appointment>()
            .Property(a => a.Status)
            .IsRequired();

        modelBuilder.Entity<Appointment>()
            .HasOne<Service>()
            .WithMany()
            .HasForeignKey(a => a.ServiceId);

        // slot

        modelBuilder.Entity<Slot>()
            .HasKey(a => a.Id);
        
        modelBuilder.Entity<Slot>()
            .Property(a => a.DateTime)
            .IsRequired();

        modelBuilder.Entity<Slot>()
            .Property(a => a.Duration)
            .IsRequired();

        modelBuilder.Entity<Slot>()
            .Property(a => a.Position)
            .IsRequired();

        modelBuilder.Entity<Slot>()
            .HasOne<Appointment>()     
            .WithMany()
            .HasForeignKey(a => a.AppointmentId);            

        base.OnModelCreating(modelBuilder);
    }
}

}