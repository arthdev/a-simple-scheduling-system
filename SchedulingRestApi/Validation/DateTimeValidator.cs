using System;
using System.Collections.Generic;
using System.Globalization;
using FluentValidation;
using FluentValidation.Validators;
using Microsoft.Extensions.Localization;

namespace Scheduling.Validation
{
    

public class DateTimeValidator<T> : PropertyValidator<T, string> {
	public override bool IsValid(ValidationContext<T> context, string attempetdValue) {
           DateTime date;
        var result = DateTime.TryParseExact(
            attempetdValue,
            new string[] {
                "yyyy'-'MM'-'dd' 'HH':'mm':'ss",
                "yyyy'-'MM'-'dd'T'HH':'mm':'ss",
                "yyyy'-'MM'-'dd",
            },
            DateTimeFormatInfo.InvariantInfo,
            DateTimeStyles.RoundtripKind,
            out date
        );
        return result;
	}

    public override string Name => "DateTimeValidator";

	protected override string GetDefaultMessageTemplate(string errorCode)
		=> this.Localized(errorCode, nameof(DateTimeValidator<object>));
}
}