using System;
using FluentValidation.Validators;
using Microsoft.Extensions.Localization;

namespace Scheduling.Validation
{
    public class AppLanguageManager : FluentValidation.Resources.LanguageManager 
    {
        public AppLanguageManager() {
            OverrideDefaultMessages();
            AddCustomValidatorsTranslations();
        }

        private void AddCustomValidatorsTranslations()
        {
            AddTranslation("en-US", nameof(IntegerValidator<object>), 
                "'{PropertyName}' must be valid integer number.");
            AddTranslation("en-US", nameof(DateTimeValidator<object>), 
                "'{PropertyName}' must be valid date and time.");
            AddTranslation("en-US", nameof(StartDateValidator<object>), 
                "'{PropertyName}' minutes and '{ComparasionProperty}' must be multiple of 5.");
            AddTranslation("en-US", nameof(RequiredIf<object>), 
                "'{PropertyName}' is required if '{Property}' if informed.");

            AddTranslation("pt-BR", nameof(IntegerValidator<object>), 
                "'{PropertyName}' deve ser um número inteiro válido.");
            AddTranslation("pt-BR", nameof(DateTimeValidator<object>), 
                "'{PropertyName}' deve ser uma data e hora válida.");
            AddTranslation("pt-BR", nameof(StartDateValidator<object>), 
                "Os minutos da'{PropertyName}' e '{ComparasionProperty}' devem ser múltiplos de 5.");
            AddTranslation("pt-BR", nameof(RequiredIf<object>), 
                "'{PropertyName}' é obrigatório se '{Property}' for informado.");
        }

        private void OverrideDefaultMessages()
        {
            AddTranslation("pt-BR", nameof(NotNullValidator<object, object>), 
                "'{PropertyName}' é obrigatório.");
            AddTranslation("pt-BR", nameof(NotEmptyValidator<object, object>), 
                "'{PropertyName}' é obrigatório.");
        }
    }
}