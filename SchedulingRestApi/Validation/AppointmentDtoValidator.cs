using System;
using System.Globalization;
using FluentValidation;
using Microsoft.Extensions.Localization;
using Scheduling.Requests;

namespace Scheduling.Validation
{
    public class AppointmentDtoValidator : AbstractValidator<AppointmentDto>
    {
        public AppointmentDtoValidator(IStringLocalizer localizer)
        {
            RuleFor(a => a.StartDateTime).NotNull();

            RuleFor(a => a.StartDateTime).Must((model, value, context) => {
                if (value.HasValue && (value.Value.Minute % 5) != 0) {
                    return false;
                }

                return true;
            }).WithMessage(_ => localizer["'{PropertyName}' must be at a slot start."]);;

            RuleFor(a => a.StartDateTime).Must((model, value, context) => {
                /* some custom, specific rule using another property value */
                if (model.RequiresAccessibility.HasValue && model.RequiresAccessibility.Value)
                {
                    if (value.Value.TimeOfDay.CompareTo(new TimeSpan(12, 0, 0)) >= 0) {
                        context.MessageFormatter.AppendArgument(
                            "RequiresAccessibility", localizer[nameof(model.RequiresAccessibility)]);
                        return false;
                    }
                }

                return true;
                /* 
                    example: message formatting for the validator using custom placeholders ("RequiresAccessibility") 
                    and localization.
                */
            }).WithMessage(_ => localizer["'{PropertyName}' must be before noon if \"{RequiresAccessibility}\" is checked."]);

            // custom reusable rule using localization features, placeholders, another fields of model.
            RuleFor(a => a.RequiresAccessibility).SetValidator(
                new RequiredIf<AppointmentDto>("StartDateTime", localizer));
        }
    }
}