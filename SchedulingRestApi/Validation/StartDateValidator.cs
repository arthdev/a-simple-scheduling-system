using System;
using System.Collections.Generic;
using System.Globalization;
using FluentValidation;
using FluentValidation.Validators;
using Microsoft.Extensions.Localization;
using Scheduling.Utils;

namespace Scheduling.Validation
{
    

public class StartDateValidator<T> : PropertyValidator<T, DateTime?> {
    private readonly string _comparationProperty;
    private IStringLocalizer localizer;

    public StartDateValidator(string comparationProperty, IStringLocalizer localizer)
    {
        this._comparationProperty = comparationProperty;
        this.localizer = localizer;
    }
    public override bool IsValid(ValidationContext<T> context, DateTime? attempted) {
        if (context.InstanceToValidate.GetType().GetProperty(_comparationProperty) != null)
        {
            var compoarasionValue = context.InstanceToValidate
                .GetType()
                .GetProperty(_comparationProperty)
                .GetValue(context.InstanceToValidate);

            int? duration = compoarasionValue != null ? Converters.convertToInteger(compoarasionValue.ToString()) : null;

            if (attempted.HasValue && duration.HasValue)
            {
                if (attempted.Value.AddMinutes(duration.Value).Minute % 5 != 0)
                {
                    context.MessageFormatter.AppendArgument("ComparasionProperty", 
                        this.localizer[_comparationProperty]);
                    return false;
                }
            }

        }

        return true;
	}

    public override string Name => "StartDateValidator";

	protected override string GetDefaultMessageTemplate(string errorCode)
		=> this.Localized(errorCode, nameof(StartDateValidator<object>));
}

}