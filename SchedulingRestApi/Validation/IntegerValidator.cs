using System.Collections.Generic;
using System.Globalization;
using FluentValidation;
using FluentValidation.Validators;
using Microsoft.Extensions.Localization;

namespace Scheduling.Validation
{
    

public class IntegerValidator<T> : PropertyValidator<T, string> {
	public override bool IsValid(ValidationContext<T> context, string attempetdValue) {
        if (string.IsNullOrEmpty(attempetdValue))
        {
            return true;
        }
        
        int integer;
        var result = int.TryParse(
            attempetdValue,
            NumberStyles.Integer,
            NumberFormatInfo.InvariantInfo,
            out integer
        );
        return result;
	}

    public override string Name => "IntegerValidator";

	protected override string GetDefaultMessageTemplate(string errorCode)
		=> this.Localized(errorCode, nameof(IntegerValidator<object>));
}

}