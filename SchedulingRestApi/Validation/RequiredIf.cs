using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Validators;
using Microsoft.Extensions.Localization;

namespace Scheduling.Validation
{
    public class RequiredIf<T> : PropertyValidator<T, bool?>
    {
        private readonly string _property;
        private readonly IStringLocalizer _localizer;

        public override string Name => "RequiredIf";

        public RequiredIf(string property, IStringLocalizer localizer)
        {
            _property = property;
            _localizer = localizer;
        }

        public override bool IsValid(ValidationContext<T> context, bool? value)
        {
            var properyValue = context.InstanceToValidate
                .GetType()
                .GetProperty(_property)
                .GetValue(context.InstanceToValidate);

            if (properyValue != null && !value.HasValue) {
                context.MessageFormatter.AppendArgument("Property", 
                    _localizer[_property]);
                return false;
            }

            return true;
        }

        protected override string GetDefaultMessageTemplate(string errorCode)
		    => this.Localized(errorCode, nameof(RequiredIf<object>));
    }
}