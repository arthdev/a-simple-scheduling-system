docker run -it --name rabbitmq-scheduling -p 5672:5672 -p 15672:15672 rabbitmq:3.10-management
docker container start rabbitmq-scheduling -a
docker container stop rabbitmq-scheduling 

docker container exec -it rabbitmq-scheduling bash
rabbitmqctl list_queues name durable messages
avaliable columns (like durable and messages):
	auto_delete, consumer_capacity, consumer_utilisation, consumers, disk_reads, disk_writes, durable, exclusive, exclusive_consumer_pid, exclusive_consumer_tag, head_message_timestamp, leader, members, memory, message_bytes, message_bytes_persistent, message_bytes_ram, message_bytes_ready, message_bytes_unacknowledged, messages, messages_persistent, messages_ram, messages_ready, messages_ready_ram, messages_unacknowledged, messages_unacknowledged_ram, name, online, owner_pid, pid, policy, slave_pids, state, synchronised_slave_pids, type
