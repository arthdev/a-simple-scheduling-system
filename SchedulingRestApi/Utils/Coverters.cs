using System;
using System.Globalization;

namespace Scheduling.Utils 
{


public static class Converters
{
    public static DateTime? convertToDateTime(string value) 
    {
        if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            DateTime? date = DateTime.ParseExact(
                value.Trim(),
                new string[] {
                    "yyyy'-'MM'-'dd' 'HH':'mm':'ss",
                    "yyyy'-'MM'-'dd'T'HH':'mm':'ss",
                    "yyyy'-'MM'-'dd",
                },
                DateTimeFormatInfo.InvariantInfo,
                DateTimeStyles.RoundtripKind
            );

            return date;
    }

     public static decimal? convertToDecimal(string value) {
        return null;
    }

     public static int? convertToInteger(string value) 
     {
        if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            
            int? number = int.Parse(
                value.Trim(),
                NumberStyles.Integer,
                NumberFormatInfo.InvariantInfo
            );

            return number;
    }
}

}