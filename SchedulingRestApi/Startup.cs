using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.AspNetCore;
using Localization.Localizer;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Scheduling.Infra;
using Scheduling.Validation;
using Scheduling.Requests;
using Scheduling.Mapping;

namespace Scheduling
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers()
                .AddJsonOptions(o => {
                    o.JsonSerializerOptions.Converters.Add(new Scheduling.Requests.DateTimeConverter("yyyy-MM-dd HH:mm:ss"));
                })
                .AddFluentValidation(c => c.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "SimpleSchedulingSystem", Version = "v1" });
            });

            services.AddDbContext<AppDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("DBCS"))
            );
            // fix breaking changes (.net 6)
            // https://stackoverflow.com/questions/69961449/net6-and-datetime-problem-cannot-write-datetime-with-kind-utc-to-postgresql-ty
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            AppContext.SetSwitch("Npgsql.DisableDateTimeInfinityConversions", true);
            
            services.AddScoped<DBConnectionFactory>(ctx => new DBConnectionFactory(Configuration.GetConnectionString("DBCS")));

            services.AddAutoMapper(typeof(AppProfile).Assembly);
            
            services.AddSingleton<IStringLocalizerFactory, InMemoryStringLocalizerFactory>();
            services.AddTransient<IStringLocalizer, InMemoryStringLocalizer>();
            
            services.AddMediatR(typeof(Startup));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            ConfigureFluentValidation(app.ApplicationServices);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SimpleSchedulingSystem v1"));
            } else {
                app.UseHttpsRedirection();
            }


            app.UseRequestLocalization((o) => {
                var cultures = new string[] {
                    "pt-BR",
                    "en-US"
                };
                o.AddSupportedUICultures(cultures);
                o.AddSupportedCultures(cultures);
                o.SetDefaultCulture(cultures[0]);
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void ConfigureFluentValidation(IServiceProvider serviceProvider)
        {
            var stringLocalizer = serviceProvider.GetRequiredService<IStringLocalizer>();
            ValidatorOptions.Global.DisplayNameResolver = (type, member, expression) => {
                if (member != null) {
                    return stringLocalizer.GetString(member.Name);
                }
                return null;
            };

            ValidatorOptions.Global.LanguageManager = new AppLanguageManager();
        }
    }
}
