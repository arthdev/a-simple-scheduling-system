using System;
using AutoMapper;
using Scheduling.CQRS.Commands;
using Scheduling.Entities;
using Scheduling.Requests;

namespace Scheduling.Mapping
{
    public class AppProfile : Profile
    {
        public AppProfile()
        {
            CreateMap<string, decimal?>().ConvertUsing<DecimalConverter>();
            CreateMap<string, DateTime?>().ConvertUsing<DateTimeConverter>();
            CreateMap<string, int?>().ConvertUsing<IntegerConverter>();
            
            CreateMap<AppointmentDto, CreateAppointment>()
                .ForMember(command => command.ServiceId, opt => opt.MapFrom(dto => dto.Service));
            CreateMap<CreateAppointment, Appointment>();
        }
    }
}
