using AutoMapper;
using System.Globalization;

namespace Scheduling.Mapping
{
    public class IntegerConverter : ITypeConverter<string, int?>
    {
        public int? Convert(string source, int? destination, ResolutionContext context)
        {
            if (string.IsNullOrEmpty(source))
            {
                return null;
            }
            
            int? number = int.Parse(
                source.Trim(),
                NumberStyles.Integer,
                NumberFormatInfo.InvariantInfo
            );

            return number;
        }
    }
}