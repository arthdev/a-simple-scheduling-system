using AutoMapper;
using System.Globalization;

namespace Scheduling.Mapping
{
    public class DecimalConverter : ITypeConverter<string, decimal?>
    {
        public decimal? Convert(string source, decimal? destination, ResolutionContext context)
        {
            if (string.IsNullOrEmpty(source))
            {
                return null;
            }
            
            decimal? number = decimal.Parse(
                source.Trim(),
                NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint,
                NumberFormatInfo.InvariantInfo
            );

            return number;
        }
    }
}