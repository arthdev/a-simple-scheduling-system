using System;

namespace Scheduling.Entities
{
    

public class Service
{
    public int Id { get; set; }
    public int? Duration { get; set; }
}

}