using System;

namespace Scheduling.Entities
{
    

public class Appointment
{
    public int Id { get; set; }
    public DateTime? StartDateTime { get; set; }
    public int? Duration { get; set; }
    public int? ServiceId { get; set; }
    public int Status { get; set; }
}

}