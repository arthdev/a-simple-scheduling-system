namespace Scheduling.Entities
{
    public enum AppointmentStatus
    {
        NOT_CONFIRMED = 1,
        CONFIRMED = 2,
        CANCELED = 3,
    }
}