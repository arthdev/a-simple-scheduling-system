using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Localization.Resources
{
    public static class PtBrStrings
    {
        public const string Culture = "pt-BR";
        private static readonly IReadOnlyDictionary<string, string> _strings = 
            new ReadOnlyDictionary<string, string>(
                new Dictionary<string, string>() 
                {
                    { "Description", "Descrição" },
                    { "StartDateTime", "Data de Início" },
                    { "EndDateTime", "Data de Término" },
                    { "Duration", "Duração" },
                    { "Service", "Serviço" },
                    { "RequiresAccessibility", "Requer Acessibilidade" },
                    { "'{PropertyName}' must be before noon if \"{RequiresAccessibility}\" is checked.",
                        "'{PropertyName}' deve ser antes do meio dia se \"{RequiresAccessibility}\" for marcado."},
                    {"'{PropertyName}' must be at a slot start.",
                        "'{PropertyName}' deve ser no início de uma vaga."}
                }
            );

        public static IReadOnlyDictionary<string, string> Strings 
        { 
            get 
            {
                return _strings;
            }
        }
    }
}