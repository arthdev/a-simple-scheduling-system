using System.Text.Json;
using System.Text.Json.Serialization;
using System.Globalization;
using System;

namespace Scheduling.Requests
{
	public class DateTimeConverter : JsonConverter<DateTime>
	{
		private readonly string Format;
		public DateTimeConverter(string format)
		{
			Format = format;
		}
		public override void Write(Utf8JsonWriter writer, DateTime date, JsonSerializerOptions options)
		{
			writer.WriteStringValue(date.ToString(Format));
		}
		public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			DateTime dateTime;
			DateTimeStyles styles = DateTimeStyles.AllowLeadingWhite 
				| DateTimeStyles.AllowTrailingWhite
				| DateTimeStyles.RoundtripKind;
			bool result = DateTime.TryParseExact(reader.GetString(), Format, null, styles, out dateTime);
			
			if (!result) {
				throw new Exception("Invalid date format '" + reader.GetString() + "'");
			}

			return dateTime;
		}
	}
}
