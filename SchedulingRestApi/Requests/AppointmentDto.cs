namespace Scheduling.Requests
{
    public class AppointmentDto
    {
        public System.DateTime? StartDateTime { get; set; }
        public int? Service { get; set; }
        public bool? RequiresAccessibility { get; set; }
    }
}