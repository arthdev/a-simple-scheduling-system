﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Scheduling.CQRS;
using Scheduling.CQRS.Commands;
using Scheduling.CQRS.Queries;
using Scheduling.CQRS.Responses;
using Scheduling.Entities;
using Scheduling.Requests;

namespace Scheduling.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AppointmentController : ControllerBase
    {
        private readonly ILogger<AppointmentController> _logger;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public AppointmentController(
            ILogger<AppointmentController> logger, IMapper mapper, IMediator mediator)
        {
            _logger = logger;
            _mapper = mapper;
            _mediator = mediator;
        }

        [HttpGet]
        public Task<PaginatedResult<AppointmentDetails>> Get([FromQuery] int size, [FromQuery] int page)
        {            
            return _mediator.Send(new SearchAppointments(size, page));
        }

        [HttpPost]
        public Task<string> Post([FromBody] AppointmentDto dto)
        {         
            return _mediator.Send(_mapper.Map<AppointmentDto, CreateAppointment>(dto));
        }

        [HttpGet("{id}")]
        public Task<AppointmentDetails> Get([FromRoute] string id)
        {         
            var query = new GetAppointment();
            query.Id = int.Parse(id);

            return _mediator.Send(query);
        }
    }
}
