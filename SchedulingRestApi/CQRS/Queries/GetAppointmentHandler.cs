using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Dapper;
using Scheduling.CQRS.Responses;
using Scheduling.Infra;

namespace Scheduling.CQRS.Queries
{
    
public class GetAppointmentHandler : QueryHandler<GetAppointment, AppointmentDetails>
{
    private readonly DBConnectionFactory _factory;

    public GetAppointmentHandler(DBConnectionFactory factory)
    {
        this._factory = factory;
    }

    protected override AppointmentDetails HandleQuery(GetAppointment query)
    {
        var sql = "SELECT ";
        sql += "a.\"Id\", TO_CHAR(\"StartDateTime\", 'YYYY-MM-DD hh:mm:ss') AS \"StartDateTime\", a.\"Duration\", a.\"ServiceId\", a.\"Status\" ";
        sql += "FROM \"Appoitments\" AS a ";
        sql += "WHERE \"Id\" = @Id";
        var connection = _factory.GetOpenConnection();
        var queryParams = new DynamicParameters();
        queryParams.Add("Id", query.Id, System.Data.DbType.Int32);
        IEnumerable<AppointmentDetails> a = connection.Query<AppointmentDetails>(sql, queryParams);
        return a.Single();
    }  
}

}