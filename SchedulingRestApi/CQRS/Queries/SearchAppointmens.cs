using Scheduling.CQRS.Responses;

namespace Scheduling.CQRS.Queries
{
    public class SearchAppointments : PaginatedQuery<AppointmentDetails>
    {
        public SearchAppointments(int size, int page) : base(size, page)
        {
        }
    }
}