using System;
using Scheduling.CQRS.Responses;

namespace Scheduling.CQRS.Queries
{
    public abstract class PaginatedQuery<T> : Query<PaginatedResult<T>>
    {
        public int Page { get; set; }
        public int Size { get; set; }

        public PaginatedQuery(int size, int page)
        {
            Size = Math.Min(Math.Max(size, 1), 5); // max 5 itens per page
            Page = Math.Max(page, 1); // min page 1
        }
    }
}