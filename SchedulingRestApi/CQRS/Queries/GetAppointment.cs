using Scheduling.CQRS.Responses;

namespace Scheduling.CQRS.Queries
{
public class GetAppointment : Query<AppointmentDetails>
{
    public int? Id { get; set; }
}
}