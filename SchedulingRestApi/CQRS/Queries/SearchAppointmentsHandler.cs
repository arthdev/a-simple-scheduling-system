using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Dapper;
using Scheduling.CQRS.Responses;
using Scheduling.Entities;
using Scheduling.Infra;

namespace Scheduling.CQRS.Queries
{
    public class SearchAppointmentsHandler : QueryHandler<SearchAppointments, PaginatedResult<AppointmentDetails>>
    {
        private readonly DBConnectionFactory _factory;

        public SearchAppointmentsHandler(DBConnectionFactory factory)
        {
            this._factory = factory;
        }

        protected override PaginatedResult<AppointmentDetails> HandleQuery(SearchAppointments query)
        {
            int size = query.Size;
            int offset = (query.Page * query.Size) - query.Size;
            var selectSql = " SELECT ";
            selectSql += " a.\"Id\", TO_CHAR(\"StartDateTime\", 'YYYY-MM-DD hh:mm:ss') AS \"StartDateTime\", a.\"Duration\", a.\"ServiceId\", a.\"Status\" ";

            var baseSql = " FROM \"Appoitments\" AS a ";
            var sortSql = " ORDER BY \"Id\" ASC ";

            var paginationSql = $" OFFSET {offset} LIMIT {size} ";
            var connection = _factory.GetOpenConnection();

            var sql = selectSql + baseSql + sortSql + paginationSql;

            var result = connection.QuerySingle(" SELECT COUNT(*) AS total " + baseSql);
            long total = (long) result.total;
            IEnumerable<AppointmentDetails> a = connection.Query<AppointmentDetails>(sql);
            return new PaginatedResult<AppointmentDetails>(a, total);
        }  
    }
}