using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace Scheduling.CQRS.Queries
{
public abstract class QueryHandler<TQuery, TResult> : RequestHandler<TQuery, TResult> 
    where TQuery : Query<TResult>
{
    protected override TResult Handle(TQuery request)
    {
        return this.HandleQuery(request);
    }

    protected abstract TResult HandleQuery(TQuery query);
}
}