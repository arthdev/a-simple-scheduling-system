using System.Collections.Generic;

namespace Scheduling.CQRS.Responses
{
    public class PaginatedResult<T>
    {
        public IEnumerable<T> Result { get; set; }
        public long Total { get; set ;}

        public PaginatedResult(IEnumerable<T> result, long total)
        {
            Result = result;
            Total = total;
        }
    }
}