namespace Scheduling.CQRS.Responses
{
    public class AppointmentDetails
    {
        public string Id { get; set; }
        public string StartDateTime { get; set; }
        public string Duration { get; set; }
        public string ServiceId { get; set; }
        public string Status { get; set; }
    }    
}