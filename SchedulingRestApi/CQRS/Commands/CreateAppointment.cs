using System;

namespace Scheduling.CQRS.Commands
{
public class CreateAppointment : Command<string>
{
    public DateTime? StartDateTime { get; set; }
    public int? ServiceId { get; set; }
    public bool? RequiresAccessibility { get; set; }
}
}