using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace Scheduling.CQRS.Commands
{
public abstract class CommandHandler<TCommand, TResult> : RequestHandler<TCommand, TResult> 
    where TCommand : Command<TResult>
{
    protected override TResult Handle(TCommand request)
    {
        return this.HandleCommand(request);
    }

    protected abstract TResult HandleCommand(TCommand command);
}
}