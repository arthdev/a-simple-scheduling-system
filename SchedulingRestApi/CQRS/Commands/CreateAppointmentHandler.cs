using AutoMapper;
using Scheduling.Entities;
using Scheduling.Events;
using Scheduling.Infra;

namespace Scheduling.CQRS.Commands
{
public class CreateAppointmentHandler : CommandHandler<CreateAppointment, string>
{
    private readonly AppDbContext _context;
    private readonly IMapper _mapper;

    public CreateAppointmentHandler(AppDbContext context, IMapper mapper)
    {
        this._context = context;
        this._mapper = mapper;
    }

    protected override string HandleCommand(CreateAppointment command)
    {
        var appointment = _mapper.Map<CreateAppointment, Appointment>(command);
        appointment.Status = (int) AppointmentStatus.NOT_CONFIRMED;

        Service service = _context.Services.Find(command.ServiceId) ?? throw new System.Exception("Service not found.");

        // @TODO: enqueue event and publish inside a worker
        // _messageBus.Publish(
            new AppointmentRequested()
            {
                Id = System.Guid.NewGuid(),
                AppointmentId = appointment.Id,
                StartDateTime = appointment.StartDateTime.Value,
                // TODO: find the service and set the duration accordingly
                Duration = service.Id,
                ServiceId = appointment.ServiceId,
            };
        // );
        
        var entry = _context.Add(appointment);
        _context.SaveChanges();

        return entry.Entity.Id.ToString();
    }  
}
}