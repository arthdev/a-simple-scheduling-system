using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MessageBus
{
    public interface IMessageBus
    {
        EventHandler<object> OnPublishConfirm { get; set; }
        void Publish(object message, string destination);
        void StartConsuming();
        void Subscribe<T>(Action<T> action, string source);
        void Unsubscribe<T>(Action<T> action, string source);
    }
}