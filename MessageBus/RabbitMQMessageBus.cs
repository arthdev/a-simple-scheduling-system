using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using Polly;
using Polly.Retry;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;

namespace MessageBus
{
    public class RabbitMQMessageBus : IDisposable, IMessageBus
    {
        public EventHandler<object> OnPublishConfirm { get; set; }
        private readonly PersistentConnection _persistentConnection;
        private readonly int _retryCount;
        private IModel _consumerChannel;
        private string _queueName;
        private string _brokerName;
        private readonly IDictionary<string, ICollection<object>> _subscriptions;
        public RabbitMQMessageBus(PersistentConnection connection, int retryCount, 
            string queueName, string brokerName)
        {
            _persistentConnection = connection;
            _retryCount = retryCount;
            _subscriptions = new Dictionary<string, ICollection<object>>();

            _queueName = queueName;
            _brokerName = brokerName;
        }
        public void Publish(object message, string routingKey)
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            var policy = RetryPolicy
                .Handle<BrokerUnreachableException>()
                .Or<SocketException>()
                .WaitAndRetry(_retryCount, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
                {
                    Console.WriteLine("Could not publish message: {MessageId} after {Timeout}s ({ExceptionMessage})", @message, $"{time.TotalSeconds:n1}", ex.Message);
                });

            using (var channel = _persistentConnection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: _brokerName, type: "direct");

                var jsonMessage = JsonSerializer.Serialize(message, message.GetType());
                var body = Encoding.UTF8.GetBytes(jsonMessage);

                policy.Execute(() =>
                {
                    var props = channel.CreateBasicProperties();
                    props.Persistent = true;

                    channel.ConfirmSelect();

                    channel.BasicAcks += (obj, ea) => 
                    {
                        OnPublishConfirm?.Invoke(this, @message);
                        Console.WriteLine("Message ID {0} published.", @message);
                    };

                    channel.BasicPublish(exchange: _brokerName,
                        routingKey: routingKey,
                        basicProperties: props,
                        body: body);

                    channel.WaitForConfirmsOrDie();
                });
            }
        }
        public void Subscribe<T>(Action<T> action, string routingKey)
        {
            if (!_subscriptions.ContainsKey(routingKey))
            {
                _subscriptions.Add(routingKey, new List<object>());
            }
            _subscriptions[routingKey].Add(action);

            OnMessageSubscriptionAdded(routingKey);
        }
        public void Unsubscribe<T>(Action<T> action, string routingKey)
        {
            if (_subscriptions.ContainsKey(routingKey))
            {
                _subscriptions[routingKey].Remove(action);
            }

            OnMessageSubscriptionRemoved(routingKey);
        }
        public void StartConsuming()
        {
            _consumerChannel = CreateConsumerChannel();
        }
        public void Dispose()
        {
            _consumerChannel?.Dispose();
        }
        private void OnMessageSubscriptionAdded(string routingKey)
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            using (var channel = _persistentConnection.CreateModel())
            {
                channel.QueueBind(
                    queue: _queueName,
                    exchange: _brokerName,
                    routingKey: routingKey
                );
            }
        }
        private void OnMessageSubscriptionRemoved(string routingKey)
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            using (var channel = _persistentConnection.CreateModel())
            {
                if (!_subscriptions.ContainsKey(routingKey)) {
                    channel.QueueUnbind(
                        queue: _queueName,
                        exchange: _brokerName,
                        routingKey: routingKey
                    );
                }

                if (!_subscriptions.Any())
                {
                    _queueName = string.Empty;
                    _consumerChannel.Close();
                }
            }
        }
        private IModel CreateConsumerChannel()
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            var channel = _persistentConnection.CreateModel();

            channel.ExchangeDeclare(exchange: _brokerName, type: "direct");

            channel.QueueDeclare(queue: _queueName,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var routingKey = ea.RoutingKey;

                try {
                    var rawMessage = Encoding.UTF8.GetString(ea.Body.ToArray());
                    object message = null;

                    foreach (var subscription in GetSubscriptionsForMessage(routingKey))
                    {
                        Type type = subscription.GetType();
                        if (message == null)                        
                        {                   
                            Type messageType = type.GetGenericArguments()[0];
                            message = JsonSerializer.Deserialize(rawMessage, messageType);
                        }

                        type.GetMethod("Invoke").Invoke(subscription, new object[] { message });      
                    }

                    channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                } catch (Exception e) {
                    Console.WriteLine("Error when receive message: {0}", e.Message);
                }
            };

            channel.BasicConsume(queue: _queueName, autoAck: false, consumer: consumer);

            channel.CallbackException += (sender, ea) =>
            {
                _consumerChannel.Dispose();
                _consumerChannel = CreateConsumerChannel();
            };

            return channel;
        }       
        private IEnumerable<object> GetSubscriptionsForMessage(string routingKey)
        {
            if (_subscriptions.ContainsKey(routingKey)) {
                return _subscriptions[routingKey];
            }
            return Enumerable.Empty<object>();
        }  
    }
}
