using System;

namespace MessageBus 
{
    public abstract class Message<T>
    {
        public T payload { get; set; }
        public Guid Id { get; set; }
    }
}