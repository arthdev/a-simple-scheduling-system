using Microsoft.EntityFrameworkCore;
using Scheduler.Entities;

namespace Scheduler.Infra
{
    

public class AppDbContext : DbContext
{
    public DbSet<Slot> Slots { get; set; }
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {

    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Slot>()
            .HasKey(a => a.Id);
        
        modelBuilder.Entity<Slot>()
            .Property(a => a.DateTime)
            .IsRequired();

        modelBuilder.Entity<Slot>()
            .Property(a => a.Duration)
            .IsRequired();

        modelBuilder.Entity<Slot>()
            .Property(a => a.Position)
            .IsRequired();

        modelBuilder.Entity<Slot>()      
            .Property(a => a.AppointmentId);

        base.OnModelCreating(modelBuilder);
    }
}

}