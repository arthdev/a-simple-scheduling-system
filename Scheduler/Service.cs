using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MessageBus;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Scheduler.Entities;
using Scheduler.Events;
using Scheduler.Infra;
using Scheduler.Services;

namespace Scheduler
{    
    public class Service : BackgroundService
    {
        private readonly ILogger<Service> _logger;
        private readonly SlotService _slotService;
        private readonly RabbitMQMessageBus _messageBus;
        private readonly IServiceProvider _services;
        private const string AppointmentRequestedRoutingKey = "appointment_requested";
        private const string AppointmentConfirmedRoutingKey = "appointment_confirmed";
        private const string AppointmentRejectedRoutingKey = "appointment_rejected";
        public Service(IServiceProvider services, ILogger<Service> logger, SlotService slotService,
            RabbitMQMessageBus messageBus)
        {
            _services = services;
            _logger = logger;
            _slotService = slotService;
            _messageBus = messageBus;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Service is running.");
            await DoWork(stoppingToken);
        }

        private async Task DoWork(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Service is working.");

            // TODO: log that message was published (outbox pattern)
            // _messageBus.OnPublishConfirm = (@event, message) => {
            // };

            _messageBus.StartConsuming();
            _logger.LogInformation("Started consuming from rabbitmq.");

            _messageBus.Subscribe<AppointmentRequested>(m => 
                {
                    AppointmentRequested eventt = m;
                    _logger.LogInformation("Confirming appointment.");
                    try
                    {
                        _slotService.ConfirmAppointment(eventt);
                        _logger.LogInformation("Slots assigned to appointment!");
                        // TODO: apply outbox pattern 
                        _messageBus.Publish(new AppointmentConfirmed() {
                            Id = Guid.NewGuid(),
                            AppointmentId = eventt.AppointmentId,
                        }, AppointmentConfirmedRoutingKey);
                        // TODO: log event handling (deduplication event handling)
                    }
                    catch (System.Exception e)
                    {
                        _logger.LogInformation(e, e.Message);
                        _logger.LogInformation("Slots can't be assigned!");
                        // TODO: apply outbox pattern 
                        _messageBus.Publish(new AppointmentRejected() {
                            Id = Guid.NewGuid(),
                            AppointmentId = eventt.AppointmentId,
                            Reason = e.Message,
                        }, AppointmentRejectedRoutingKey);
                        // TODO: log event handling (deduplication event handling)
                    }
                },
                AppointmentRequestedRoutingKey
            );

            while (!stoppingToken.IsCancellationRequested)
            {               
                await Task.Delay(10000, stoppingToken);
            }
        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Service is stopping.");
            await base.StopAsync(stoppingToken);
        }

        private IEnumerable<AppointmentRequested> GetEvents() 
        {
            IEnumerable<AppointmentRequested> events;
            using (var scope = _services.CreateScope())
            {                 
                using (var factory = scope.ServiceProvider.GetRequiredService<DBConnectionFactory>())
                {                     
                    var sql = "SELECT * FROM \"Appoitments\" AS a WHERE a.\"Status\" = 1 ORDER BY a.\"Id\" ASC LIMIT 5";
                    events = factory.GetOpenConnection().Query<AppointmentRequested>(sql);
                }
            }
            return events;           
        }
    }
}