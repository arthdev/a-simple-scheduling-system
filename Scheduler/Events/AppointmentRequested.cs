using System;
using MessageBus;

namespace Scheduler.Events
{
    public class AppointmentRequested
    {
        public Guid Id { get; set; }
        public int AppointmentId { get; set; }
        public DateTime StartDateTime { get; set; }
        public int? Duration { get; set; }
        public int? ServiceId { get; set; }
        public int Status { get; set; }
    }
}