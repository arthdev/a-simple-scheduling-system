using System;
using MessageBus;

namespace Scheduler.Events
{
    public class AppointmentRejected
    {
        public Guid Id { get; set; }
        public int AppointmentId { get; set; }
        public string Reason { get; set; }
    }
}