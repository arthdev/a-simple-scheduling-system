using System;
using MessageBus;

namespace Scheduler.Events
{
    public class AppointmentConfirmed
    {
        public Guid Id { get; set; }
        public int AppointmentId { get; set; }
    }
}