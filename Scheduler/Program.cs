﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using MessageBus;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Scheduler.Entities;
using Scheduler.Events;
using Scheduler.Infra;
using Scheduler.Services;

namespace Scheduler
{
    class Program
    {
        private static string cs = "Host=localhost;Database=scheduling;Username=agthoffmann;Password=123123";
        private static string mq = "localhost";
        private static string queueName = "scheduler";
        private static string brokerName = "simple_scheduling_system_exchange";
        private static IHost _host;
        static void Main(string[] args)
        {
            _host = Host.CreateDefaultBuilder(args)
                .ConfigureServices((context, services) => {
/* Passing CS via env. variable:
DOTNET_CONNECTIONSTRINGS__APP="Host=localhost;Database=scheduling;Username=agthoffmann;Password=123123" dotnet run --project Scheduler/Scheduler.csproj
Console.WriteLine(context.Configuration.GetSection("ConnectionStrings")["app"]);
https://docs.microsoft.com/en-us/dotnet/core/extensions/configuration-providers#environment-variable-configuration-provider
*/
                    services.AddDbContext<AppDbContext>(options =>
                       options.UseNpgsql(cs).LogTo(Console.WriteLine)
                    );
                    // fix breaking changes (.net 6)
                    // https://stackoverflow.com/questions/69961449/net6-and-datetime-problem-cannot-write-datetime-with-kind-utc-to-postgresql-ty
                    AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
                    AppContext.SetSwitch("Npgsql.DisableDateTimeInfinityConversions", true);

                    services.AddScoped<DBConnectionFactory>(ctx => new DBConnectionFactory(cs));
                    services.AddHostedService<Service>();
                    services.AddScoped<SlotService>();
                    services.AddScoped<IRepository<Slot, int>, SlotRepository>();

                    services.AddSingleton<PersistentConnection>(ctx => new PersistentConnection(mq));
                    services.AddSingleton<RabbitMQMessageBus>(ctx => new RabbitMQMessageBus(
                        ctx.GetRequiredService<PersistentConnection>(), 
                        5, 
                        queueName, brokerName
                    ));
                })
                .Build();

            // var choice = args[0];
            // Execute(choice, args);

            _host.Run();
        }

        static void Execute(string choice, string[] args)
        {
            // if (choice.Equals("seed"))
            // {
            //     SeedSlots(DateTime.Today);
            // }
            // else if (choice.Equals("list"))
            // {
            //     ListSlots(DateTime.Today);
            // }
            // else if (choice.Equals("confirm"))
            // {
            //     ConfirmAppointments();
            // }
            // else if (choice.Equals("avaliable"))
            // {
            //     ListAvaliableSlots(int.Parse(args[1]), DateTime.Now);
            // }
        }
    }
}
