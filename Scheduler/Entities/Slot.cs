using System;

namespace Scheduler.Entities
{ 
  public class Slot
  {
      public int Id { get; set; }
      public DateTime DateTime { get; set; }
      public int Duration { get; set; }
      public int Position { get; set; }
      public int? AppointmentId { get; set; }
  }  
}