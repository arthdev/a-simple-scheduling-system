using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Scheduler.Entities;
using Scheduler.Events;
using Scheduler.Infra;

namespace Scheduler.Services
{
    public class SlotService
    {
        private readonly IRepository<Slot, int> _repository;

        public SlotService(IRepository<Slot, int> repository)
        {
            this._repository = repository;
        }
        // 
        public void GenerateSlots(DateTime day)
        {
            List<Slot> slots = new List<Slot>();
            
            DateTime slotDateTime = day.AddDays(0);
            int position = 1;
            while (slotDateTime.Day.Equals(day.Day)) {
                slots.Add(
                    new Slot()
                    {
                        DateTime = slotDateTime,
                        Duration = 5,
                        Position = position,
                    }
                );

                slotDateTime = slotDateTime.AddMinutes(5);
                position++;
            }
           
            SlotCriteria criteria = new SlotCriteria();
            criteria.Day = day;

            // TODO: create Exists method in repository interface using criteria similar to Get()
            if (_repository.Get(criteria).Any())
            {
                throw new Exception("Already seeded for this day!");
            }
                
            foreach (var slot in slots)
            {
                _repository.Add(slot);
            }


            _repository.Complete();
           
            string format = "Slot seeded: {0, 4} | {1} | {2, 4} | no. {3, 4}";
            foreach (var slot in slots)
            {
                Console.WriteLine(
                    String.Format(format, slot.Id, slot.DateTime.ToString("O"), slot.Duration, slot.Position)
                );
            }
        }     

        public void ConfirmAppointment(AppointmentRequested eventt)
        {
            if ((eventt.Duration.Value % Constants.SlotDuration) != 0) {
                throw new Exception("Invalid duration!");
            }
            
            IEnumerable<Slot> avaliableSlots;          
            avaliableSlots = this.ListAvaliableSlots(eventt.StartDateTime, eventt.Duration.Value);

            int requiredQuantity = this.CalculateRequiredSlotsQuantity(eventt.Duration.Value);

            if (avaliableSlots.Count().Equals(requiredQuantity))
            {                
                foreach (var slot in avaliableSlots)
                {
                    slot.AppointmentId = eventt.AppointmentId;
                }
            }
            else
            {
                throw new Exception("There are not slots enough avaliable for this appointment!");
            }

            _repository.Complete();
        }

        private IEnumerable<Slot> ListAvaliableSlots(DateTime firstSlotStartTime, int duration)
        {
            var lastSlotStartTime = firstSlotStartTime
                .AddMinutes(duration)
                .AddMinutes(-1 * Constants.SlotDuration);

            SlotCriteria criteria = new SlotCriteria();
            criteria.IsAvaliable = true;
            criteria.MinimumDateTime = firstSlotStartTime;
            criteria.MaximumDateTime = lastSlotStartTime;

            return _repository.Get(criteria);
        }

        private int CalculateRequiredSlotsQuantity(int duration)
        {
            return (int) Math.Floor((float) duration / Constants.SlotDuration);
        }

        // public void ListAvaliableSlots(int duration, DateTime day)
        // {
        //     IEnumerable<Slot> avaliableSlots;
        //     DateTime start = day.Date;
        //     DateTime end = day.Date.AddDays(1).AddMinutes(-1);
               
        //     avaliableSlots = _dbContext.Slot.Where(s => 
        //         s.DateTime >= start
        //         && s.DateTime <= end
        //         && s.AppointmentId == null
        //     )
        //     .OrderBy(s => s.Position)
        //     .ToList();

        //     int requiredQuantity = this.CalculateRequiredSlotsQuantity(duration);
        //     int previousPosition = -1;
        //     List<Slot> sequence = new List<Slot>();
        //     foreach (var slot in avaliableSlots)
        //     {
        //         if (!slot.Position.Equals(previousPosition + 1))
        //         {
        //             sequence.Clear();
        //         }

        //         sequence.Add(slot);

        //         if (sequence.Count().Equals(requiredQuantity))
        //         {
        //             Console.WriteLine("Avaliable Slots found ({0} minutes): ", duration);
        //             PrintSlots(sequence);
        //             sequence.Clear();
        //         }

        //         previousPosition = slot.Position;
        //     }
        // }

        // public void PrintSlots(IEnumerable<Slot> slots)
        // {
        //      string format = "Slot {0,4}: at {1} | {2} minutes | no. {3}.";
        //     foreach (var slot in slots)
        //     {
        //         Console.WriteLine(
        //             String.Format(format, 
        //                 slot.Id, 
        //                 slot.DateTime.ToString("O"), 
        //                 slot.Duration, 
        //                 slot.Position)
        //         );
        //     }
        // }

        // public void ListSlots(DateTime day)
        // // {
        // //     IEnumerable<Slot> slots;
         
        // //     slots = _dbContext.Slot.Where(s => 
        // //         s.DateTime.Day.Equals(day.Day)
        // //         && s.DateTime.Month.Equals(day.Month)
        // //         && s.DateTime.Year.Equals(day.Year)
        // //     )
        // //     .OrderBy(s => s.Position)
        // //     .ToList();
           
        // //     string format = "Slot {0, 4} | {1} | {2, 4} | no. {3, 4} | appointment: {4, 4}";
        // //     foreach (var slot in slots)
        // //     {
        // //         Console.WriteLine(
        // //             String.Format(format, slot.Id, slot.DateTime.ToString("O"), slot.Duration, slot.Position, slot.AppointmentId)
        // //         );
        // //     }
        // // }
    }
}