using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Scheduler.Entities;

public class SlotCriteria : ICriteria<Slot>
{
    public bool? IsAvaliable { get; set; }
    public DateTime? MinimumDateTime { get; set ;}
    public DateTime? MaximumDateTime { get; set ;}
    public DateTime? Day { get; set ;}
    public IQueryable<Slot> ApplyToQuery(IQueryable<Slot> query)
    {
        var filteredQuery = query;
        
        foreach (var expresssion in GetExpressions())
        {
            filteredQuery = filteredQuery.Where(expresssion);
        }

        return filteredQuery;
    }

    public IEnumerable<Slot> ApplyToEnumerable(IEnumerable<Slot> items)
    {
        var filteredItems = items;
        
        foreach (var expresssion in GetExpressions())
        {
            filteredItems = filteredItems.Where(expresssion.Compile());
        }

        return filteredItems;
    }

    public List<Expression<Func<Slot, bool>>> GetExpressions() {
        List<Expression<Func<Slot, bool>>> list = new List<Expression<Func<Slot, bool>>>();
        
        if (IsAvaliable.HasValue && IsAvaliable.Value) {
            list.Add(s => s.AppointmentId == null);
        } else if (IsAvaliable.HasValue && !IsAvaliable.Value) {
            list.Add(s => s.AppointmentId != null);
        }

        if (MinimumDateTime.HasValue) {
            DateTime first = MinimumDateTime.Value;
            list.Add(s => s.DateTime >= first);
        }

        if (MaximumDateTime.HasValue) {
            DateTime last = MaximumDateTime.Value;
            list.Add(s => s.DateTime <= last);
        }

        if (Day.HasValue) {
            DateTime day = Day.Value;
            list.Add(s => 
                s.DateTime.Day.Equals(day.Day)
                && s.DateTime.Month.Equals(day.Month)
                && s.DateTime.Year.Equals(day.Year));
        }

        return list;
    }    
}