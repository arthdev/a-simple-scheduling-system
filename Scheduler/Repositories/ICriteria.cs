using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

public interface ICriteria<T>
{
    IQueryable<T> ApplyToQuery(IQueryable<T> query);
    IEnumerable<T> ApplyToEnumerable(IEnumerable<T> items);
    List<Expression<Func<T, bool>>> GetExpressions();
}