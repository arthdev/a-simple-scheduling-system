using System.Collections.Generic;
using System.Linq;
using Scheduler.Entities;
using Scheduler.Infra;

public class SlotRepository : IRepository<Slot, int>
{
    private readonly AppDbContext _dbContext;

    public SlotRepository(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public Slot Add(Slot entity)
    {
        return _dbContext.Slots.Add(entity).Entity;
    }

    public void Delete(int id)
    {
        _dbContext.Slots.Remove(_dbContext.Slots.Find(id));
    } 

    public Slot Get(int id)
    {
        return _dbContext.Slots.Find(id);
    }

    public Slot Update(Slot entity)
    {
        return _dbContext.Slots.Update(entity).Entity;
    }

    public IEnumerable<Slot> Get(ICriteria<Slot> criteria)
    {
        var query = _dbContext.Slots.AsQueryable();
        query = criteria.ApplyToQuery(query);
        return query.ToList();
    }

    public void Complete()
    {
        _dbContext.SaveChanges();
    }
}