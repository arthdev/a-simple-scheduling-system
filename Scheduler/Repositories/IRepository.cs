using System.Collections.Generic;

public interface IRepository<T, TId>
{
    T Add(T entity);
    T Update(T entity);
    void Delete (TId id);
    IEnumerable<T> Get(ICriteria<T> criteria);
    T Get(TId id);
    // TODO: implementer Unit of Work Pattern
    void Complete();
}