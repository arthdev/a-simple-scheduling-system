using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Scheduling.Infra;
using System.Text.Json;
using Microsoft.Extensions.Options;
using Scheduling;
using Microsoft.AspNetCore.Mvc;
using Scheduling.Entities;
using System.Net.Http.Json;

namespace SchedulingRestApi.Tests;
public class TestingWebAppFactory<TEntryPoint> : WebApplicationFactory<Program> where TEntryPoint : Program
{
    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        builder.ConfigureServices(services =>
        {
            var connectionString = "Host=localhost;Database=scheduling_tests;Username=agthoffmann;Password=123123";
            var descriptor = services.SingleOrDefault(
                d => d.ServiceType == typeof(DbContextOptions<AppDbContext>));

            if (descriptor != null) {
                services.Remove(descriptor);
            }
            
            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseNpgsql(connectionString);
            });

            descriptor = services.SingleOrDefault(
                d => d.ServiceType == typeof(DBConnectionFactory));

            if (descriptor != null) {
                services.Remove(descriptor);
            }
            
            services.AddScoped<DBConnectionFactory>(ctx => new DBConnectionFactory(connectionString));
        });
    }

    public void CleanupDatabase()
    {        
        using (var scope = this.Services.CreateScope()) {
            var dbcontext = (AppDbContext?) scope.ServiceProvider.GetService(typeof(AppDbContext));
            if (dbcontext != null) {
                /*
                https://learn.microsoft.com/en-us/ef/core/managing-schemas/ensure-created
                EnsureCreated and Migrate do not work together. 
                "The EnsureCreated() and EnsureDeleted() methods provide a lightweight alternative to Migrations for managing the database schema. 
                These methods are useful in scenarios when the data is transient and can be dropped when the schema changes. 
                For example during prototyping, in tests, or for local caches."
                https://stackoverflow.com/a/38241900
                */
                dbcontext.Database.EnsureDeleted();
                dbcontext.Database.EnsureCreated();
            }
        }
    }

    public JsonSerializerOptions? GetJsonSerializationOptions()
    {
        var options = (IOptions<JsonOptions>?) this.Services.GetService(typeof(IOptions<JsonOptions>));

        if (options != null) {
            return options.Value.JsonSerializerOptions;
        }

        return null;
    }

    public JsonContent CreateJsonContent(object content)
    {
        var json = JsonContent.Create(content, content.GetType(), null, this.GetJsonSerializationOptions());
        return json;
    }

    public void AddService(Service service)
    {
        this.AddServices(new [] { service });
    }

    public void AddServices(IEnumerable<Service> services)
    {
        using (var scope = this.Services.CreateScope()) 
        {
            var dbcontext = (AppDbContext?) scope.ServiceProvider.GetService(typeof(AppDbContext)) 
                ?? throw new Exception("AppDbContext not found");
                
            dbcontext.Services.AddRange(services);
            dbcontext.SaveChanges();
        }
    }

    public void AddAppointments(int quantity)
    {
        Service service = new Service() {
            Duration = 25
        };
        
        this.AddService(service);

        List<Appointment> appointments = new List<Appointment>(quantity);

        for (int i = 0; i < quantity; i++)
        {
            appointments.Add(
                new Appointment() {
                    Duration = service.Duration,
                    ServiceId = service.Id,
                    Status = (int) AppointmentStatus.NOT_CONFIRMED,
                    StartDateTime = DateTime.Now.AddHours(i),
                }
            );
        }

        using (var scope = this.Services.CreateScope()) 
        {
            var dbcontext = (AppDbContext?) scope.ServiceProvider.GetService(typeof(AppDbContext)) 
                ?? throw new Exception("AppDbContext not found");
                
            dbcontext.Appoitments.AddRange(appointments);
            dbcontext.SaveChanges();
        }
    }
}