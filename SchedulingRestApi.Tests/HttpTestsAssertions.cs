using System.Net.Http.Json;
using System.Text.Json.Nodes;
using Xunit;

namespace SchedulingRestApi.Tests
{
    public class HttpTestsAssertions
    {
        public static async Task AssertHasValidationError(string key, HttpResponseMessage response)
        {
            var content = await response.Content.ReadFromJsonAsync<JsonNode>();
            Assert.NotNull(content);
            var body = content!.AsObject();
            var errors = body["errors"]!.AsObject();
            Assert.NotNull(errors);
            Assert.True(errors!.ContainsKey(key));
        }

        public static async Task AssertPagination(int pageSize, int totalItems, HttpResponseMessage response)
        {
            var content = await response.Content.ReadFromJsonAsync<JsonNode>();
            Assert.NotNull(content);
            var body = content!.AsObject();
            var result = body["result"]!.AsArray();
            Assert.Equal(pageSize, result.Count);
            var total = body["total"]!.AsValue();
            Assert.Equal(totalItems, total.GetValue<int>());
        }
    }
}