using System.Net.Http.Json;
using Scheduling.Requests;
using Xunit;
using Scheduling;
using Scheduling.Entities;

namespace SchedulingRestApi.Tests;
public class IntegrationTest : IClassFixture<TestingWebAppFactory<Program>>
{
    private readonly HttpClient _client;
    private TestingWebAppFactory<Program> _webApp;

    public IntegrationTest(TestingWebAppFactory<Program> factory)
    {
        _webApp = factory;
        _client = factory.CreateClient();

        _webApp.CleanupDatabase();
    }

    [Fact]
    public async Task PostAppointment_StartIsNull_ReturnError()
    {        
        var dto = this.CreateValidAppointmentRequest();
        dto.StartDateTime = null;
        var response = await _client.PostAsync("/appointment", CreateJsonContent(dto));

        Assert.False(response.IsSuccessStatusCode);
        await HttpTestsAssertions.AssertHasValidationError("StartDateTime", response);
    }

    [Fact]
    public async Task PostAppointment_StartIsNotAtASlotStart_ReturnError()
    {
        var dto = this.CreateValidAppointmentRequest();
        dto.StartDateTime = new DateTime(2023,01,11,15,01,00);
        var response = await _client.PostAsync("/appointment", CreateJsonContent(dto));

        Assert.False(response.IsSuccessStatusCode);
        await HttpTestsAssertions.AssertHasValidationError("StartDateTime", response);
    }

    [Fact]
    public async Task PostAppointment_RequiresAccessibilityAndStartIsNotBeforeNoon_ReturnError()
    {
        var dto = this.CreateValidAppointmentRequest();
        dto.StartDateTime = new DateTime(2023,01,11,12,00,00);
        dto.RequiresAccessibility = true;
        var response = await _client.PostAsync("/appointment", CreateJsonContent(dto));

        Assert.False(response.IsSuccessStatusCode);
        await HttpTestsAssertions.AssertHasValidationError("StartDateTime", response);
    }

    [Fact]
    public async Task PostAppointment_RequiresAccessibilityAndStartIsBeforeNoon_ReturnSuccess()
    {
        var dto = this.CreateValidAppointmentRequest();
        dto.StartDateTime = new DateTime(2023,01,11,11,55,00);;
        dto.RequiresAccessibility = true;
        var response = await _client.PostAsync("/appointment", CreateJsonContent(dto));

        Assert.True(response.IsSuccessStatusCode);
    }
    
    [Fact]
    public async Task GetAppointments_IfPageSet_ReturnGivenPage()
    {
        var total = 5;
        this._webApp.AddAppointments(total);

        var size = 2;

        var page = 1;
        var totalPages = Math.Ceiling((decimal) total/size);
        while (page <= totalPages)
        {
            bool isLastPage = (page == totalPages);
            int pageSize = isLastPage ? (total % size) : size;

            var response = await _client.GetAsync($"/appointment?page={page}&size={size}");

            Assert.True(response.IsSuccessStatusCode);
            await HttpTestsAssertions.AssertPagination(pageSize, total, response);
            page++;
        }


    }

    private AppointmentDto CreateValidAppointmentRequest()
    {
        var dto = new AppointmentDto();
        dto.StartDateTime = new DateTime(2023,01,11,15,00,00);
        dto.RequiresAccessibility = false;

        Service service = new Service() { Duration = 25 };
        this._webApp.AddService(service);

        dto.Service = service.Id;
        return dto;
    }

    private JsonContent CreateJsonContent(object content)
    {
        return this._webApp.CreateJsonContent(content);
    }
}